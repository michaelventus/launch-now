//
//  ImageServiceDummy.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

@testable import LaunchNow

/// Mocked out ImageNetworking
final class ImageServiceDummy: ImageNetworking {

    var shouldFail: Bool = false
    var dataToReturn: Data = Data()

    func fetchImageData(from urlString: String) -> Single<Data> {
        return Single<Data>.create { single in
            if self.shouldFail {
                single(.error(RxError.unknown))
            } else {
                single(.success(self.dataToReturn))
            }
            return Disposables.create()
        }
    }
}
