//
//  RocketLaunchDisplaySpy.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

@testable import LaunchNow

final class RocketLaunchDisplaySpy: RocketLaunchDisplay {

    var screenTitle: String?
    var rocketImageData: Data?
    var description: String?
    var location: String?
    var status: String?
    var date: String?
    var interest: String?

    var imageIsHidden: Bool?
    var descriptionIsHidden: Bool = false
    var interestIsHidden: Bool?

    func set(title: String) {
        screenTitle = title
    }

    func update(rocketImage: Data) {
        rocketImageData = rocketImage
    }

    func update(description: String) {
        self.description = description
    }

    func update(location: String) {
        self.location = location
    }

    func update(status: String) {
        self.status = status
    }

    func update(date: String) {
        self.date = date
    }

    func update(interest: String) {
        self.interest = interest
    }

    func hideImage() {
        imageIsHidden = true
    }

    func hideDescriptionLabel() {
        descriptionIsHidden = true
    }

    func hideFlaggedLabel() {
        interestIsHidden = true
    }
}
