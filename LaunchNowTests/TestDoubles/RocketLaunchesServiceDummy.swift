//
//  RocketLaunchesServiceDummy.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

@testable import LaunchNow

/// Mocked out RocketLaunchesNetworking
final class RocketLaunchesServiceDummy: RocketLaunchesNetworking {

    var launchesShouldFail: Bool = false
    var launchesToReturn: RocketLaunches = RocketLaunches(results: [])

    func fetchLaunches() -> Single<RocketLaunches> {
        return Single<RocketLaunches>.create { single in
            if self.launchesShouldFail {
                single(.error(RxError.unknown))
            } else {
                single(.success(self.launchesToReturn))
            }
            return Disposables.create()
        }
    }

    var rocketShouldFail: Bool = false
    var rocketToReturn: RocketInfo = RocketInfo(imageUrl: "http://www.google.com")

    func fetchRocket(from url: String) -> Single<RocketInfo> {
        return Single<RocketInfo>.create { single in
            if self.rocketShouldFail {
                single(.error(RxError.unknown))
            } else {
                single(.success(self.rocketToReturn))
            }
            return Disposables.create()
        }
    }
}
