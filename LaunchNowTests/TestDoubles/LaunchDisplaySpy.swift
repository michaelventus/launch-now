//
//  LaunchDisplaySpy.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

@testable import LaunchNow

final class LaunchDisplaySpy: LaunchCellDisplay {

    var name: String?
    var location: String?
    var imageData: Data?
    var imageCleared: Bool = false
    var cellHasBeenFlagged: Bool?

    func update(name: String, location: String) {
        self.name = name
        self.location = location
    }

    func update(image: Data) {
        imageData = image
    }

    func clearCellImage() {
        imageData = nil
        imageCleared = true
    }

    func flagCell() {
        cellHasBeenFlagged = true
    }

    func unFlagCell() {
        cellHasBeenFlagged = false
    }
}
