//
//  RocketLaunchesDisplaySpy.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

@testable import LaunchNow

final class RocketLaunchesDisplaySpy: RocketLaunchesDisplay {

    var screenTitle: String?
    var errorViewIsHidden: Bool?
    var loaderIsHidden: Bool?
    var tableIsSetup: Bool?

    var launches: [RocketLaunch] = []
    var viewingRocket: RocketLaunch?

    func set(title: String) {
        screenTitle = title
    }

    func showErrorView() {
        errorViewIsHidden = false
    }

    func hideErrorView() {
        errorViewIsHidden = true
    }

    func showLoader() {
        loaderIsHidden = false
    }

    func hideLoader() {
        loaderIsHidden = true
    }

    func setupTable() {
        tableIsSetup = true
    }

    func updateLaunchesList(with launches: [RocketLaunch]) {
        self.launches = launches
    }

    func viewFullDetails(of rocketLaunch: RocketLaunch) {
        viewingRocket = rocketLaunch
    }
}
