//
//  RocketLaunchTests.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import LaunchNow

class RocketLaunchTests: QuickSpec {

    override func spec() {

        let chinaLaunch = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                location: Location(name: "Australia",
                                                                   countryCode: "CHN")),
                                       status: Status(name: Launch(rawValue: "TBD")!),
                                       date: "",
                                       mission: Mission(description: "Fight the aliens"),
                                       rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))

        let russiaLaunch = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                 location: Location(name: "Australia",
                                                                    countryCode: "RUS")),
                                        status: Status(name: Launch(rawValue: "TBD")!),
                                        date: "",
                                        mission: Mission(description: "Fight the aliens"),
                                        rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))

        let unknownLaunch = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                  location: Location(name: "Australia",
                                                                     countryCode: "UNK")),
                                         status: Status(name: Launch(rawValue: "TBD")!),
                                         date: "",
                                         mission: Mission(description: "Fight the aliens"),
                                         rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))

        let australiaLaunch = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                    location: Location(name: "Australia",
                                                                       countryCode: "AUS")),
                                           status: Status(name: Launch(rawValue: "TBD")!),
                                           date: "",
                                           mission: Mission(description: "Fight the aliens"),
                                           rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))


        describe("RocketLaunch model") {
            it("determines if a launch should be flagged") {
                expect(chinaLaunch.isOfParticularInterest).to(beTrue())
                expect(russiaLaunch.isOfParticularInterest).to(beTrue())
                expect(unknownLaunch.isOfParticularInterest).to(beTrue())
                expect(australiaLaunch.isOfParticularInterest).to(beFalse())
            }
        }
    }
}
