//
//  LaunchPresenterTests.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Quick
import Nimble

@testable import LaunchNow

typealias Pad = RocketLaunches.RocketLaunch.Pad
typealias Status = RocketLaunches.RocketLaunch.Status
typealias Mission = RocketLaunches.RocketLaunch.Mission
typealias Location = RocketLaunches.RocketLaunch.Pad.Location
typealias Launch = RocketLaunches.RocketLaunch.Status.Launch
typealias Rocket = RocketLaunches.RocketLaunch.Rocket
typealias Configuration = RocketLaunches.RocketLaunch.Rocket.Configuration

class LaunchPresenterTests: QuickSpec {

    override func spec() {

        var display: LaunchDisplaySpy!
        var presenter: LaunchPresenter!
        var imageService: ImageServiceDummy!
        var rocketService: RocketLaunchesServiceDummy!
        let rocketLaunch = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                 location: Location(name: "Australia",
                                                                    countryCode: "AUS")),
                                        status: Status(name: Launch(rawValue: "TBD")!),
                                        date: "",
                                        mission: Mission(description: "Fight the aliens"),
                                        rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))

        let rocketLaunchOfInterest = RocketLaunch(pad: Pad(name: "Mike's Home",
                                                 location: Location(name: "Australia",
                                                                    countryCode: "CHN")),
                                        status: Status(name: Launch(rawValue: "TBD")!),
                                        date: "",
                                        mission: Mission(description: "Fight the aliens"),
                                        rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil)))
        
        describe("LaunchPresenter") {
            beforeEach {
                display = LaunchDisplaySpy()
                imageService = ImageServiceDummy()
                rocketService = RocketLaunchesServiceDummy()
            }

            context("launch not of interest") {
                beforeEach {
                    presenter = LaunchPresenter(display: display,
                                                rocketLaunch: rocketLaunch,
                                                imageService: imageService,
                                                rocketService: rocketService)
                    presenter.viewDidLoad()
                }

                it("clears out any previous image data on load") {
                    expect(display.imageCleared).to(beTrue())
                }

                it("sets the correct cell name") {
                    expect(display.name).to(equal("Mike's Rocket"))
                }

                it("sets the correct cell location") {
                    expect(display.location).to(equal("Location: Australia"))
                }

                it("passes data to display from service") {
                    expect(display.imageData).toEventuallyNot(beNil())
                }

                it("does not flag the cell") {
                    expect(display.cellHasBeenFlagged).to(beFalse())
                }
            }

            context("launch is of interest") {

                beforeEach {
                    presenter = LaunchPresenter(display: display,
                                                rocketLaunch: rocketLaunchOfInterest,
                                                imageService: imageService,
                                                rocketService: rocketService)
                    presenter.viewDidLoad()
                }

                it("flags cells when a launch is of interest") {
                    expect(display.cellHasBeenFlagged).to(beTrue())
                }
            }
        }
    }
}
