//
//  RocketLaunchesPresenterTests.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Quick
import Nimble

@testable import LaunchNow

class RocketLaunchesPresenterTests: QuickSpec {

    override func spec() {

        var presenter: RocketLaunchesPresenting!
        var display: RocketLaunchesDisplaySpy!
        var service: RocketLaunchesServiceDummy!

        let launches: RocketLaunches = RocketLaunches(results: [RocketLaunch(pad: Pad(name: "Mike's Home",
                                                                                      location: Location(name: "Australia",
                                                                                                         countryCode: "AUS")),
                                                                             status: Status(name: Launch(rawValue: "TBD")!),
                                                                             date: "",
                                                                             mission: Mission(description: "Fight the aliens"),
                                                                             rocket: Rocket(configuration: Configuration(name: "Mike's Rocket", url: "http://www.google.com", imageUrl: nil))),
                                                                RocketLaunch(pad: Pad(name: "Mike's Kitchen",
                                                                                      location: Location(name: "New Zeland",
                                                                                                         countryCode: "NZ")),
                                                                             status: Status(name: Launch(rawValue: "TBD")!),
                                                                             date: "",
                                                                             mission: Mission(description: "Fight the aliens"),
                                                                             rocket: Rocket(configuration: Configuration(name: "Mike's second Rocket", url: "http://www.google.com", imageUrl: nil)))])

        

        describe("RocketLaunchesPresenter") {

            beforeEach {
                display = RocketLaunchesDisplaySpy()
                service = RocketLaunchesServiceDummy()
            }

            context("initial launch") {
                beforeEach {
                    service.launchesToReturn = launches
                    presenter = RocketLaunchesPresenter(display: display, service: service)
                    presenter.viewDidLoad()
                }

                it("sets display title correctly") {
                    expect(display.screenTitle).to(equal("Upcoming launches"))
                }

                it("hides error view") {
                    expect(display.errorViewIsHidden).to(beTrue())
                }


                it("sets up table on launch") {
                    expect(display.tableIsSetup).to(beTrue())
                }

                it("passes the display a list of rocket launches") {

                    expect(display.launches.count).toEventually(equal(2))

                    let firstLaunch = display.launches[0]
                    let secondLaunch = display.launches[1]

                    expect(firstLaunch.rocket.configuration.name).to(equal("Mike's Rocket"))
                    expect(secondLaunch.rocket.configuration.name).to(equal("Mike's second Rocket"))
                }

                it("hides the loader") {
                    expect(display.loaderIsHidden).to(beTrue())
                }
            }

            context("error handling") {
                beforeEach {
                    service.launchesShouldFail = true
                    presenter = RocketLaunchesPresenter(display: display, service: service)
                    presenter.viewDidLoad()
                }

                it("should tell the display to present the error state") {
                    expect(display.errorViewIsHidden).toEventually(beFalse())
                }
            }
        }
    }
}
