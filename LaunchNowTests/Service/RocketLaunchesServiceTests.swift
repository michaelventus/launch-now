//
//  RocketLaunchesServiceTests.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import Quick
import Nimble
import OHHTTPStubs
import RxSwift

@testable import LaunchNow

class RocketLaunchesServiceTests: QuickSpec {

    override func spec() {

        describe("RocketLaunchesService") {

            let disposeBag = DisposeBag()

            it("returns back list of rocket launches") {

                let responseMocked = self.expectation(description: "response mocked")

                stub(condition: isHost("spacelaunchnow.me")) { _ in
                    let stubPath = OHPathForFile("launches.json", type(of: self))
                    responseMocked.fulfill()
                    return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
                }

                let expectedResponse = self.expectation(description: "fetch launches")

                RocketLaunchesService().fetchLaunches()
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { rocketLaunches in
                        expectedResponse.fulfill()
                        expect(rocketLaunches.results.count).to(equal(2))
                        guard let rocketLaunch = rocketLaunches.results.first else {
                            fail("Unexpected failure")
                            return
                        }

                        expect(rocketLaunch.rocket.configuration.name).to(equal("Falcon 9 Block 5"))
                        expect(rocketLaunch.rocket.configuration.imageUrl).to(beNil())
                        expect(rocketLaunch.rocket.configuration.url).to(equal("https://spacelaunchnow.me/api/3.3.0/config/launcher/164/?format=json"))

                        expect(rocketLaunch.mission?.description).to(equal("Second operational Starlink payload of 2020 and third overall. The Falcon 9 launch vehicle will carry a batch of 60 Starlink satellites that will be insterted in a Low Earth Orbit (LEO) at an altitude of 290 km (180 mi) and an inclination of 53°. They will then split into three orbital planes and raise their orbits to reach an operational altitude of 550 km (342 mi). The Starlink LEO constellation aims to provide worldwide affordable satellite internet access. 182 satellites (120 operational) have been deployed so far, the goal scheduled for the mid-2020s being 12,000 satellites in orbit, with a possible later extension to 42,000. The booster for this launch will be B1051, a Block 5 generation core which previously flew on SpaceX's Demonstration Mission 1 (first Crew Dragon test flight) and launched the Canadian Space Agency's RADARSAT Constellation."))

                        expect(rocketLaunch.pad.location.name).to(equal("Cape Canaveral, FL, USA"))
                        expect(rocketLaunch.pad.location.countryCode).to(equal("USA"))
                        expect(rocketLaunch.pad.name).to(equal("Space Launch Complex 40"))

                        expect(rocketLaunch.status.name).to(equal(Launch.go))

                        expect(rocketLaunch.date).to(equal("2020-01-28T01:49:00+11:00"))

                    }, onError: { _ in
                        fail("Unexpected failure")
                    }).disposed(by: disposeBag)

                self.wait(for: [expectedResponse, responseMocked], timeout: 5)
            }

            it("returns error") {

                let responseMocked = self.expectation(description: "response mocked")

                stub(condition: isHost("spacelaunchnow.me")) { _ in
                    responseMocked.fulfill()
                    return fixture(filePath: "", status: 500, headers: ["Content-Type":"application/json"])
                }

                let expectedResponse = self.expectation(description: "fetch launches")

                RocketLaunchesService().fetchLaunches()
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { rocketLaunches in
                        fail("Unexpected success")
                    }, onError: { _ in
                        expectedResponse.fulfill()
                    }).disposed(by: disposeBag)

                self.wait(for: [expectedResponse, responseMocked], timeout: 5)
            }

            it("returns back an individual rocket") {

                let responseMocked = self.expectation(description: "response mocked")

                stub(condition: isHost("spacelaunchnow.me.rocket")) { _ in
                    let stubPath = OHPathForFile("rocket.json", type(of: self))
                    responseMocked.fulfill()
                    return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
                }

                let expectedResponse = self.expectation(description: "fetch rocket")

                RocketLaunchesService().fetchRocket(from: "https://spacelaunchnow.me.rocket/api/3.3.0/config/launcher/164/?format=json")
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { rocket in
                        expectedResponse.fulfill()
                        expect(rocket.imageUrl).to(equal("https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/falcon25209_image_20190224025007.jpeg"))

                    }, onError: { _ in
                        fail("Unexpected failure")
                    }).disposed(by: disposeBag)

                self.wait(for: [expectedResponse, responseMocked], timeout: 5)
            }
        }
    }
}
