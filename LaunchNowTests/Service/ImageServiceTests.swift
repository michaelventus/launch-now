//
//  ImageServiceTests.swift
//  LaunchNowTests
//
//  Created by Michael Ventus on 25/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import Quick
import Nimble
import OHHTTPStubs
import RxSwift

@testable import LaunchNow

class ImageServiceTests: QuickSpec {

    override func spec() {

        describe("ImageService") {

            let disposeBag = DisposeBag()

            it("returns back data for a given rocket url") {

                let responseMocked = self.expectation(description: "response mocked")

                stub(condition: isHost("spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com")) { _ in
                    let stubPath = OHPathForFile("rocket.jpeg", type(of: self))
                    responseMocked.fulfill()
                    return fixture(filePath: stubPath!, headers: ["Content-Type":"application/json"])
                }

                let expectedResponse = self.expectation(description: "fetch image")

                ImageService().fetchImageData(from: "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/falcon25209_image_20190224025007.jpeg")
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { imageData in
                        expectedResponse.fulfill()
                        expect(imageData).toNot(beNil())
                    }, onError: { _ in
                        fail("Unexpected failure")
                    }).disposed(by: disposeBag)

                self.wait(for: [expectedResponse, responseMocked], timeout: 5)
            }

            it("returns error") {

                let responseMocked = self.expectation(description: "response mocked")

                stub(condition: isHost("spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com")) { _ in
                    responseMocked.fulfill()
                    return fixture(filePath: "", status: 500, headers: ["Content-Type":"application/json"])
                }

                let expectedResponse = self.expectation(description: "fetch image")

                ImageService().fetchImageData(from: "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/falcon25209_image_20190224025007.jpeg")
                    .observeOn(MainScheduler.instance)
                    .subscribe(onSuccess: { imageData in
                        fail("Unexpected success")
                    }, onError: { _ in
                        expectedResponse.fulfill()
                    }).disposed(by: disposeBag)

                self.wait(for: [expectedResponse, responseMocked], timeout: 5)
            }
        }
    }
}
