//
//  RocketLaunchesService.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

/// Networking for rocket related services
protocol RocketLaunchesNetworking {
    /// Retrieves `RocketLaunches` from service
    func fetchLaunches() -> Single<RocketLaunches>

    /// Retrieves `RocketInfo` from a given url `String`
    func fetchRocket(from url: String) -> Single<RocketInfo>
}

final class RocketLaunchesService: RocketLaunchesNetworking {

    // MARK: - RocketLaunchesNetworking

    /// Fetch all launch information
    func fetchLaunches() -> Single<RocketLaunches> {
        var urlRequest: URLRequest =  URLRequest(url: Configuration.absoluteUrl(for: Endpoint.rocketLaunches.rawValue))
        urlRequest.httpMethod = "GET"

        return HTTPClient().fetchSingleObject(request: urlRequest)
    }

    /// Fetch individual details for a rocket (only place that seems to contain image for a rocket)
    func fetchRocket(from url: String) -> Single<RocketInfo> {
        var urlRequest: URLRequest =  URLRequest(url: Configuration.absoluteUrl(for: url))
        urlRequest.httpMethod = "GET"

        return HTTPClient().fetchSingleObject(request: urlRequest)
    }
}
