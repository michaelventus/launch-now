//
//  ObservableDatasource.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

/// Datasource which returns back observables
protocol ObservableDatasource {
    /// Invokes a service to return a `Single<T>` decodable object
    func fetchSingleObject<T>(request: URLRequest) -> Single<T> where T: Decodable

    /// Invokes a service to return a `Data` object
    func fetchSingleDataObject(request: URLRequest) -> Single<Data>
}


// Future improvement: - figure out elegant way to merge the 2 functions in this class
final class HTTPClient: ObservableDatasource {

    func fetchSingleObject<T>(request: URLRequest) -> Single<T> where T: Decodable {
        return Single<T>.create { single in
            let session = URLSession.shared

            session.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    single(.error(error))
                    return
                }

                // Future improvement: - return different error types depending on status code
                guard let httpResponse = response as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode),
                    let data = data else {
                        single(.error(RxError.noElements))
                        return
                }

                do {
                    let json = try JSONDecoder().decode(T.self, from: data)
                    single(.success(json))
                } catch {
                    single(.error(error))
                }

            }).resume()

            return Disposables.create()
        }
    }

    func fetchSingleDataObject(request: URLRequest) -> Single<Data> {
        return Single<Data>.create { single in
            let session = URLSession.shared

            session.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    single(.error(error))
                    return
                }

                // Future improvement: - return different error types depending on status code
                guard let httpResponse = response as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode),
                    let data = data else {
                        single(.error(RxError.noElements))
                        return
                }

                single(.success(data))
            }).resume()

            return Disposables.create()
        }
    }
}
