//
//  EndpointConfiguration.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

/// Integration endpoints
/// Future improvement - to handle different environments (preprod/prod)
enum Endpoint: String {
    case rocketLaunches = "https://spacelaunchnow.me/api/3.3.0/launch/upcoming/?format=json"
}

/// Configuration  helper to return back URL object from a given string
struct Configuration {
    static func absoluteUrl(for endpoint: String) -> URL {
        guard let url = URL(string: endpoint) else {
            fatalError("Endpoint configuration error")
        }

        return url
    }
}
