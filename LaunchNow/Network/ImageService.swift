//
//  ImageService.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

/// Networking for fetching image data
protocol ImageNetworking {
    /// Fetch image data given a url `String`
    func fetchImageData(from urlString: String) -> Single<Data>
}

final class ImageService: ImageNetworking {
    func fetchImageData(from urlString: String) -> Single<Data> {
        var urlRequest: URLRequest =  URLRequest(url: Configuration.absoluteUrl(for: urlString))
        urlRequest.httpMethod = "GET"

        return HTTPClient().fetchSingleDataObject(request: urlRequest)
    }
}
