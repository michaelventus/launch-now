//
//  UIColor.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import UIKit

extension UIColor {
    public class var flaggedRed: UIColor {
        return UIColor(named: "flaggedRed")!
    }
}
