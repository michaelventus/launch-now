//
//  RocketLaunchViewController.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import UIKit

/// Display callbacks for the `Presenter` to invoke
protocol RocketLaunchDisplay: AnyObject {
    /// Update `Display` screen title
    func set(title: String)

    /// Update `Display` rocket image
    func update(rocketImage: Data)

    /// Update `Display` launch description
    func update(description: String)

    /// Update `Display` location
    func update(location: String)

    /// Update `Display` status
    func update(status: String)

    /// Update `Display` date
    func update(date: String)

    /// Update `Display` if the launch has been flagged of interest
    func update(interest: String)

    /// Update `Display` to hide the image
    func hideImage()

    /// Update `Display` to hide the description
    func hideDescriptionLabel()

    /// Update `Display` to hide the flag of interest
    func hideFlaggedLabel()
}

class RocketLaunchViewController: UIViewController, RocketLaunchDisplay {

    var presenter: RocketLaunchPresenting?

    @IBOutlet weak var rocketImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

    // MARK: - RocketLaunchDisplay

    func set(title: String) {
        self.title = title
    }
    
    func update(rocketImage: Data) {
        rocketImageView.image = UIImage(data: rocketImage)
    }

    func update(description: String) {
        descriptionLabel.text = description
    }

    func update(location: String) {
        locationLabel.text = location
    }

    func update(status: String) {
        statusLabel.text = status
    }

    func update(date: String) {
        dateLabel.text = date
    }

    func update(interest: String) {
        interestLabel.text = interest
    }

    func hideFlaggedLabel() {
        interestLabel.isHidden = true
    }

    func hideImage() {
        rocketImageView.removeFromSuperview()
    }

    func hideDescriptionLabel() {
        descriptionLabel.isHidden = true
    }
}
