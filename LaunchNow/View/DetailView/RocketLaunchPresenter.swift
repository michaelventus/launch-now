//
//  RocketLaunchPresenter.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

/// Presenter functions for the `Display` to invoke
protocol RocketLaunchPresenting {
    /// Inform the `Presenter` that the `Display` has loaded
    /// Handles setting up all initial state of the `Display`
    func viewDidLoad()
}

final class RocketLaunchPresenter: RocketLaunchPresenting {

    weak var display: RocketLaunchDisplay?

    private let rocketLaunch: RocketLaunch
    private let formatter = DateFormatter()

    init(display: RocketLaunchDisplay, rocketLaunch: RocketLaunch) {
        self.display = display
        self.rocketLaunch = rocketLaunch
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    }

    // MARK: - RocketLaunchPresenting

    func viewDidLoad() {

        // Future improvement: - If no cached object, then fetch image
        if let cachedImageData = imageCache.object(forKey: rocketLaunch.rocket.configuration.name as NSString) {
            DispatchQueue.main.async { [weak self] in
                self?.display?.update(rocketImage: cachedImageData as Data)
            }
        } else {
            display?.hideImage()
        }

        display?.set(title: rocketLaunch.rocket.configuration.name)

        // Show description if launch contains one
        if let description = rocketLaunch.mission?.description {
            display?.update(description: description)
        } else {
            display?.hideDescriptionLabel()
        }

        display?.update(location: "Location:-\n\(rocketLaunch.pad.name), \(rocketLaunch.pad.location.name)\n")
        display?.update(status: "Launch status:-\n\(rocketLaunch.status.name.rawValue)\n")

        // Format date - Response String -> Date -> Formatted String
        if let date = rocketLaunch.date,
            let dateFromString = formatter.date(from: date) {
            formatter.dateStyle = .long
            formatter.timeStyle = .medium

            let formattedDate = formatter.string(from: dateFromString)
            display?.update(date: "Target date:-\n\(formattedDate)\n")
        }

        // Flag launch if of interest
        if rocketLaunch.isOfParticularInterest {
            display?.update(interest: "Launch is flagged")
        } else {
            display?.hideFlaggedLabel()
        }
    }
}
