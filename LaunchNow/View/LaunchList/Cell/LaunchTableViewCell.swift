//
//  LaunchTableViewCell.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import UIKit

/// Display callbacks for the `Presenter` to invoke
protocol LaunchCellDisplay: AnyObject {
    /// Update `Display` with name and location
    func update(name: String, location: String)

    /// Update `Display` image data
    func update(image: Data)

    /// Reset `Display` image
    func clearCellImage()

    /// Update `Display` to flag the cell of particular interest
    func flagCell()

    /// Update `Display` to remove the flag of particular interest
    func unFlagCell()
}

final class LaunchTableViewCell: UITableViewCell, LaunchCellDisplay {

    @IBOutlet weak var launchImageView: UIImageView!
    @IBOutlet weak var launchNameLabel: UILabel!
    @IBOutlet weak var launchLocationLabel: UILabel!
    
    var presenter: LaunchPresenting? {
        didSet {
            presenter?.viewDidLoad()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    // MARK: - LaunchCellDisplay

    func update(name: String, location: String) {
        launchNameLabel.text = name
        launchLocationLabel.text = location
    }

    func update(image: Data) {
        launchImageView.backgroundColor = .clear
        launchImageView.image = UIImage(data: image)
    }

    func clearCellImage() {
        launchImageView.backgroundColor = .lightGray
        launchImageView.image = nil
    }

    func flagCell() {
        backgroundColor = .flaggedRed
    }

    func unFlagCell() {
        backgroundColor = .white
    }
}
