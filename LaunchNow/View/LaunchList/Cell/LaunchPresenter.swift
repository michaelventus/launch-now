//
//  LaunchPresenter.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

/// Presenter functions for the `Display` to invoke
protocol LaunchPresenting: AnyObject {
    /// Inform the `Presenter` that the `Display` has loaded
    /// Handle initial state of the `Display` and fetch the details of the rocket/ fetch image of rocket
    func viewDidLoad()
}

/// Cache to store image data that has been fetched from service
let imageCache = NSCache<NSString, NSData>()

typealias RocketLaunch = RocketLaunches.RocketLaunch

/// LaunchPresenter -> Encapsulates `LaunchTableViewCell` logic
final class LaunchPresenter: LaunchPresenting {

    weak var display: LaunchCellDisplay?

    private var rocketLaunch: RocketLaunch
    private var reused: Bool = true
    private let imageService: ImageNetworking
    private let rocketService: RocketLaunchesNetworking
    private let disposeBag = DisposeBag()

    init(display: LaunchCellDisplay,
         rocketLaunch: RocketLaunch,
         imageService: ImageNetworking = ImageService(),
         rocketService: RocketLaunchesNetworking = RocketLaunchesService()) {
        self.display = display
        self.rocketLaunch = rocketLaunch
        self.imageService = imageService
        self.rocketService = rocketService
    }

    // MARK: - LaunchPresenting
    
    func viewDidLoad() {
        display?.update(name: rocketLaunch.rocket.configuration.name,
                        location: "Location: \(rocketLaunch.pad.location.name)")

        // Use cached image if it exists
        if let cachedImageData = imageCache.object(forKey: rocketLaunch.rocket.configuration.name as NSString) {
            DispatchQueue.main.async { [weak self] in
                self?.display?.update(image: cachedImageData as Data)
            }
        } else {

            display?.clearCellImage()

            // 1. Fetch individual rocket info (No imageUrl seems to come back from the main service -> rocket payload)
            // 2. Fetch image data from rocket payload
            // 3. Update UI with image

            rocketService.fetchRocket(from: rocketLaunch.rocket.configuration.url)
                .flatMap { [weak self] rocket -> Single<Data> in
                    guard let self = self, let imageUrl = rocket.imageUrl else { throw RxError.unknown }
                    return self.imageService.fetchImageData(from: imageUrl)
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] imageData in
                guard let self = self else { return }
                imageCache.setObject(imageData as NSData, forKey: self.rocketLaunch.rocket.configuration.name as NSString)
                self.display?.update(image: imageData)
            }).disposed(by: disposeBag)
        }

        // Flag `particular interest` countries with a red cell
        if rocketLaunch.isOfParticularInterest {
            display?.flagCell()
        } else {
            display?.unFlagCell()
        }
    }
}
