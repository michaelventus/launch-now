//
//  RocketLaunchesPresenter.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation
import RxSwift

/// Presenter functions for the `Display` to invoke
protocol RocketLaunchesPresenting {
    /// Inform the `Presenter` that the `Display` has loaded
    /// Handle initial state of the `Display` and fetch the list of launches
    func viewDidLoad()

    /// Inform the `Presenter` that the `Display` wants to re-fetch the list of launches
    func viewDidTapRetry()

    /// Inform the `Presenter` that an individual cell has been tapped
    func viewDidTapCell(_ rocketLaunch: RocketLaunch)
}

/// RocketLaunchesPresenter -> Encapsulates `RocketLaunchesViewController` logic
final class RocketLaunchesPresenter: RocketLaunchesPresenting {

    weak var display: RocketLaunchesDisplay?

    private let service: RocketLaunchesNetworking
    private let disposeBag = DisposeBag()

    init(display: RocketLaunchesDisplay, service: RocketLaunchesNetworking = RocketLaunchesService()) {
        self.display = display
        self.service = service
    }

    // MARK: - RocketLaunchesPresenting

    /// Initial state of view
    func viewDidLoad() {

        // Future Improvement: - Store strings somewhere else
        display?.set(title: "Upcoming launches")
        display?.setupTable()
        display?.hideErrorView()
        display?.showLoader()
        
        fetchLaunches()
    }

    func viewDidTapRetry() {
        display?.hideErrorView()
        display?.showLoader()

        fetchLaunches()
    }

    func viewDidTapCell(_ rocketLaunch: RocketLaunch) {
        display?.viewFullDetails(of: rocketLaunch)
    }

    // MARK: - Private helpers

    private func fetchLaunches() {
        service.fetchLaunches()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] rocketLaunches in

                self?.display?.updateLaunchesList(with: rocketLaunches.results)
                self?.display?.hideLoader()
                // Future improvement: - Handle different empty list state

            }) { [weak self] error in

                self?.display?.hideLoader()
                self?.display?.showErrorView()
                // Future improvement: - Handle different types of errors differently

        }.disposed(by: disposeBag)
    }
}
