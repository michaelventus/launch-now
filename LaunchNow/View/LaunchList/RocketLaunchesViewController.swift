//
//  RocketLaunchesViewController.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import UIKit

/// Display callbacks for the `Presenter` to invoke
protocol RocketLaunchesDisplay: AnyObject {
    /// Update `Display` screen title
    func set(title: String)

    /// Setup the `Display` table
    func setupTable()

    /// Display the `Display` error view
    func showErrorView()

    /// Hide the `Display` error view
    func hideErrorView()

    /// Show `Display` loader
    func showLoader()

    /// Hide `Display` loader
    func hideLoader()

    /// Update `Display` with a new `RocketLaunch` list
    func updateLaunchesList(with launches: [RocketLaunch])

    /// Navigate the `Display` to the full details of `RocketLaunch`
    func viewFullDetails(of rocketLaunch: RocketLaunch)
}

class RocketLaunchesViewController: UIViewController, RocketLaunchesDisplay {

    @IBOutlet weak var errorView: UIStackView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var rocketLaunchesTableView: UITableView!

    private var presenter: RocketLaunchesPresenting?
    private var rocketLaunchesDataSource: RocketLaunchesDataSource = RocketLaunchesDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = RocketLaunchesPresenter(display: self)
        presenter?.viewDidLoad()
    }

    @IBAction func retryButtonTapped(_ sender: Any) {
        presenter?.viewDidTapRetry()
    }

    // MARK: - RocketLaunchesDisplay

    func set(title: String) {
        self.title = title
    }

    func setupTable() {
        rocketLaunchesTableView.register(UINib(nibName: "LaunchTableViewCell", bundle: nil), forCellReuseIdentifier: "LaunchTableViewCell")
        rocketLaunchesTableView.rowHeight = UITableView.automaticDimension
        rocketLaunchesTableView.separatorStyle = .none
        rocketLaunchesTableView.dataSource = rocketLaunchesDataSource
        rocketLaunchesTableView.delegate = self
    }

    func showErrorView() {
        errorView.isHidden = false
    }

    func hideErrorView() {
        errorView.isHidden = true
    }

    func showLoader() {
        loader.isHidden = false
        loader.startAnimating()
    }

    func hideLoader() {
        loader.stopAnimating()
        loader.isHidden = true
    }

    func updateLaunchesList(with launches: [RocketLaunch]) {
        rocketLaunchesDataSource.rocketLaunches = launches
        rocketLaunchesTableView.reloadData()
    }

    // Future Improvement: - Move these into a router and follow `VIPER` instead of plain old `MVP`
    func viewFullDetails(of rocketLaunch: RocketLaunch) {

        let detailsViewController = RocketLaunchViewController()
        let detailsPresenter = RocketLaunchPresenter(display: detailsViewController, rocketLaunch: rocketLaunch)
        detailsViewController.presenter = detailsPresenter

        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

// MARK: - UITableViewDelegate

extension RocketLaunchesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.viewDidTapCell(rocketLaunchesDataSource.rocketLaunches[indexPath.row])
    }
}

// MARK: - UITableViewDataSource

final class RocketLaunchesDataSource: NSObject, UITableViewDataSource {

    var rocketLaunches: [RocketLaunch] = []

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rocketLaunches.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LaunchTableViewCell") as? LaunchTableViewCell
            else {
                return UITableViewCell()
        }

        cell.presenter = LaunchPresenter(display: cell, rocketLaunch: rocketLaunches[indexPath.row])

        return cell
    }
}
