//
//  RocketLaunch.swift
//  LaunchNow
//
//  Created by Michael Ventus on 26/1/20.
//  Copyright © 2020 Michael Ventus. All rights reserved.
//

import Foundation

/// RocketLaunches model
struct RocketLaunches: Decodable {
    var results: [RocketLaunch]

    struct RocketLaunch: Decodable {

        var pad: Pad    // Contains location
        var status: Status
        var date: String?
        var mission: Mission?    // Contains description
        var rocket: Rocket  // Contains name and image

        // Flag a launch of particular interest if it matches either of the 3 country codes
        var isOfParticularInterest: Bool {
            return ["RUS", "CHN", "UNK"].contains(pad.location.countryCode)
        }

        struct Rocket: Decodable {
            struct Configuration: Decodable {
                var name: String
                var url: String     // Contains next service request with image location
                var imageUrl: String?

                enum CodingKeys: String, CodingKey {
                    case name, url
                    case imageUrl = "image_url"
                }
            }

            var configuration: Configuration
        }

        struct Status: Decodable {
            // Not sure if I have all types - Can't seem to access the API doc (http 500 when trying to view it)
            enum Launch: String, Decodable {
                case active = "Active"
                case go = "Go"
                case success = "Success"
                case tbd = "TBD"
            }

            var name: Launch
        }

        struct Mission: Decodable {
            var description: String
        }

        struct Pad: Decodable {
            struct Location: Decodable {
                var name: String
                var countryCode: String

                enum CodingKeys: String, CodingKey {
                    case name
                    case countryCode = "country_code"
                }
            }

            var name: String
            var location: Location
        }

        enum CodingKeys: String, CodingKey {
            case pad, status, mission, rocket
            case date = "window_start"
        }
    }
}


/// Rocket info to fetch images
struct RocketInfo: Decodable {
    var imageUrl: String?

    enum CodingKeys: String, CodingKey {
        case imageUrl = "image_url"
    }
}
