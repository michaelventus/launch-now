# Rocket Launches list App
A simple app to retrieve and display a list of upcoming launches from `https://spacelaunchnow.me/api/3.3.0/launch/upcoming/?format=json`

## Installation
- Built with Xcode 11.2
- Run the Carthage update command to install libraries from `Cartfile` - `carthage update --platform iOS`

## 3rd Party Libraries
- **`RxSwift`** - Reactive bindings from `Service` -> `Presenter`.
- **`Quick/Nimble`** - My preferred way of writing tests and expectations/assertions
- **`OHHTTPStubs`** - To stub out network requests

## Technical notes
- **MVP** - My preferred architecture.
- `Display protocols` - Represents the Views of the app. Defines how the `Presenter` can talk back to the View.
- `Presenting protocols` - Represents the Presenters of each screen. Defines how the `Display` can notify the presenter what's happening on the View.
- `Networking protocols` - Represents what services are exposed to the Presenters.
- The specified endpoint doesn't seem to return back rocket images at all. Because of this, I chain network request to fetch rocket info, then fetch the image from the new payload.

## Screenshot
![](/launches.png "")